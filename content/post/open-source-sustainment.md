---
date: "2022-10-25T00:00:00+02:00"
author: "techknowlogick"
title: "Open source sustainment and the future of Gitea"
tags: ["announcement"]
draft: false
---

With Gitea reaching 6 years old, it is time to reflect on the past, and to look forward to the future. With over 14k+ commits, 1k+ contributors, 40+ maintainers, and 300M+ Docker Hub pulls, Gitea has come a long way.

<!--more-->

Our most important goal is ensuring the long term success of the project. Over the years we have tried various ways to support maintainers and the project. Some ways we have tried include bounties, direct donations, grants, and a few others. We have found that while there have been many wonderful individuals, and a few corporations who have been incredibly generous, and we are so thankful for their support, there are a few corporations (with revenues that are greater than some countries GDP) are building on Gitea for core products without even contributing back enhancements. This is of course within the scope of the license, however prevents others from the community from also benefitting.

We’d like to announce that we have formed a company, Gitea Limited, to ensure the goals are met. Some companies are unable to contribute back to open source via sponsorship or code contribution. Many more cannot contract individuals due to internal policies. In creating this new company, we are now able to offer support to those companies who do want to give back.

The Gitea project will of course remain open source, and a community project. The company will now be able to act as a steward, and as such Lunny has transferred his domains, and trademarks to the company. So far we’ve been able to support The Blender Foundation in [their move to using Gitea](https://code.blender.org/2022/07/gitea-diaries-part-1/) for developing Blender, and a few other organizations. Also, we have already been able to offer part time employment to a few maintainers, and are looking forward to being able to expand that to full time employment, as well as hiring more maintainers. Additionally, we’re planning on establishing a fund to be able to provide support to contributors who not only contribute features, but also bug fixes, performance enhancements, and important refactors.

To preserve the community aspect of Gitea we are experimenting with creating a decentralized autonomous organization where contributors would receive benefits based on their participation such as from code, documentation, translations, and perhaps even assisting individual community members with support questions. 

If you are a company and rely on Gitea, especially for critical operations, please get in touch as we are now able to offer:

* Professional support services
* Instance hosting (SaaS)
* An enhanced enterprise version
* Training
* and more!

Look forward to another announcement soon where we will document on how we plan to *continuously* *improve* Gitea ;)
